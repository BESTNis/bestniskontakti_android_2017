package rs.bestnis.dusanknezevic.bestniskontakti.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;
import rs.bestnis.dusanknezevic.bestniskontakti.models.User;
import rs.bestnis.dusanknezevic.bestniskontakti.models.database.ContactORM;
import rs.bestnis.dusanknezevic.bestniskontakti.services.networking.ConnectivityCheck;

public class DataService {

    private static final DataService ourInstance = new DataService();
    private static final String REQUEST_URL = "http://jobfairnis.rs/kontakti_api/api.php";
    private Context mContext; //it will be ApplicationContext
    private List<Contact> contacts = new ArrayList<>();
    private ArrayList<LoginHandleInterface> listeners = new ArrayList<>();

    private DataService() {
    }

    public static DataService getInstance() {
        return ourInstance;
    }

    public Context getApplicationContext() {
        return this.mContext;
    }

    public void setApplicationContext(Context context) {
        this.mContext = context.getApplicationContext(); //idiot proof if accidentally pass activity or fragment or whatever other context
    }

    public List<Contact> getContacts() {
        return this.contacts;
    }

    public JsonArrayRequest returnRequest(JSONObject loginObj) {
        return new JsonArrayRequest(Request.Method.POST, DataService.REQUEST_URL, new JSONArray().put(loginObj), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                DataService.getInstance().handleData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DataService", "error: " + error.getMessage() + error.getCause());
            }
        });
    }

    public void handleData() {
        this.contacts = ContactORM.getAllContacts(this.mContext);
        if (this.contacts.isEmpty()) {
            for (LoginHandleInterface listener : this.listeners) {
                listener.handleLogin(false);
            }
            return;
        }
        this.sortContacts();
        for (LoginHandleInterface listener : this.listeners) {
            listener.handleLogin(true);
        }
    }

    private void sortContacts() {
        Collections.sort(this.contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
    }

    private void handleData(JSONArray response) {
        if (ConnectivityCheck.getConnectivityStatus(this.mContext)) {
            try {
                int length = response.length();
                this.contacts.clear();
                ContactORM.clearContacts(this.mContext);
                for (int i = 0; i < length; i++) {
                    JSONObject contactObj = response.getJSONObject(i);
                    if (contactObj.has("login")) {
                        for (LoginHandleInterface listener : this.listeners) {
                            listener.handleLogin(false);
                        }
                        return;
                    }
                    this.contacts.add(new Contact(contactObj.getLong("id"),
                            contactObj.getString("name"),
                            contactObj.getString("surname"),
                            contactObj.getString("nickname"),
                            contactObj.getString("email"),
                            contactObj.getString("mobileNumber"),
                            contactObj.getString("imgPath")
                    ));
                }
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        ContactORM.bulkInsertContacts(DataService.getInstance().getApplicationContext(), DataService.getInstance().getContacts());
                    }
                });
                this.sortContacts();
                for (LoginHandleInterface listener : this.listeners) {
                    listener.handleLogin(true);
                }
            } catch (JSONException e) {
                Log.d("DataService", "setData: JsonException: " + e.getMessage());
            }
        } else {
            this.contacts = ContactORM.getAllContacts(this.mContext);
        }
    }

    public void addUser(User user) {

    }

    public void registerListener(LoginHandleInterface listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void unregisterListener(LoginHandleInterface listener) {
        if (listener != null && listeners.contains(listener)) {
            listeners.remove(listeners.indexOf(listener));
        }
    }

    public interface LoginHandleInterface {
        void handleLogin(boolean isLogged);
        //void handleLogin(boolean isLogged, User user);
    }
}
