package rs.bestnis.dusanknezevic.bestniskontakti.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {
    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    private long id;
    private String name;
    private String surname;
    private String nickname;
    private String email;
    private String mobileNumber;
    private String imgUri;

    public Contact(long id, String name, String surname, String nickname, String email, String mobileNumber, String imgUri) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.nickname = nickname;
        this.email = email;
        this.mobileNumber = mobileNumber;
        setImgUri(imgUri);
    }

    /**
     * Use when reconstructing Contact object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     */
    private Contact(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.surname = in.readString();
        this.nickname = in.readString();
        this.email = in.readString();
        this.mobileNumber = in.readString();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getImgUri() {
        return imgUri;
    }

    private void setImgUri(String imgUri) {
        //TODO: Just a quick fix for contacts with no image. This needs to be handled on server. Return empty for Glide and Picasso to skip loading image
        if (imgUri != null && !imgUri.equals("http://jobfairnis.rs/portal/")) {
            this.imgUri = Uri.parse(imgUri)
                    .buildUpon()
                    .build()
                    .toString();
        } else {
            //If null or empty, Picasso and Glide libs will just skip load() method and load placeholder
            this.imgUri = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.nickname);
        dest.writeString(this.email);
        dest.writeString(this.mobileNumber);
    }

    @Override
    public String toString() {
        return this.name + this.surname + this.mobileNumber;
    }
}
