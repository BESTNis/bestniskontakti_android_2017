package rs.bestnis.dusanknezevic.bestniskontakti.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import rs.bestnis.dusanknezevic.bestniskontakti.R;
import rs.bestnis.dusanknezevic.bestniskontakti.customviews.CircleImageTransformation;
import rs.bestnis.dusanknezevic.bestniskontakti.holders.ContactViewHolder;
import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;
import rs.bestnis.dusanknezevic.bestniskontakti.services.DataService;

public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> implements FastScrollRecyclerView.SectionedAdapter, Filterable {

    private RecyclerViewOnItemClickListener mListener;
    private Filter filter;
    private List<Contact> mContacts = new ArrayList<>();

    public ContactsAdapter(RecyclerViewOnItemClickListener listener, List<Contact> contacts) {
        this.mContacts = contacts;
        this.mListener = listener;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_contact, parent, false);
        return new ContactViewHolder(card);
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return this.mContacts.get(position).toString().substring(0, 1);
    }

    @Override
    public int getItemCount() {
        return this.mContacts.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact contact = this.mContacts.get(position);
        holder.bind(contact, this.mListener);
        holder.updateUI(contact);
        Picasso.with(DataService.getInstance().getApplicationContext())
                .load(contact.getImgUri())
                .placeholder(R.drawable.contact_placeholder)
                .resize(100, 100)
                .transform(new CircleImageTransformation())
                .into(holder.mImageView);
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new AppFilter<>(this.mContacts);
        return filter;

    }

    public interface RecyclerViewOnItemClickListener {
        void onItemClick(Contact contact);
    }

    /**
     * Class for filtering in Arraylist listview. Objects need a valid
     * 'toString()' method.
     *
     * @author Tobias Schürg inspired by Alxandr
     *         (http://stackoverflow.com/a/2726348/570168)
     *
     * @version 1.1 - modified by Dusan Knezevic
     *
     *  Added option to filter via more string arguments
     *  and that object needs to match all string arguments
     *  Example: "Name Surname" --> Object needs to contain both
     *  name and surname substrings. Name and surname are being treated
     *  as separate strings. You can have any number of string/substring arguments.
     */
    private class AppFilter<T> extends Filter {

        private ArrayList<T> sourceObjects;

        public AppFilter(List<T> objects) {
            sourceObjects = new ArrayList<T>();
            synchronized (this) {
                sourceObjects.addAll(objects);
            }
        }


        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            String filterSeq = chars.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterSeq.length() > 0) {
                ArrayList<T> filter = new ArrayList<T>();

                //parse strings by empty space, tab... etc.
                String[] filterSeqs = filterSeq.split("\\s+");
                //number of filtering string params
                int numFilterStrings = filterSeqs.length;
                for (T object : sourceObjects) {

                    //get number of strings that match in object
                    int j = 0;

                    for (String filterString : filterSeqs) {
                        if (object.toString().toLowerCase().contains(filterString.toLowerCase())) {
                            j++;
                        }
                    }

                    // if all search strings match, add object
                    if (j == numFilterStrings) {
                        filter.add(object);
                    }
                }
                result.count = filter.size();
                result.values = filter;
            } else {
                // add all objects
                synchronized (this) {
                    result.values = sourceObjects;
                    result.count = sourceObjects.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // NOTE: this function is *always* called from the UI thread.
            mContacts = (ArrayList<Contact>) results.values;
            notifyDataSetChanged();
        }
    }
}
