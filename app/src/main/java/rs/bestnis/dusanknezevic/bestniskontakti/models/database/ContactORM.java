package rs.bestnis.dusanknezevic.bestniskontakti.models.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;

public final class ContactORM {
    private static final String TAG = ContactORM.class.getName();
    private static final String TABLE_NAME = "contacts";
    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String COMMA_SEP = ", ";
    private static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME_TYPE = "TEXT";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SURNAME_TYPE = "TEXT";
    private static final String COLUMN_SURNAME = "surname";
    private static final String COLUMN_NICKNAME_TYPE = "TEXT";
    private static final String COLUMN_NICKNAME = "nickname";
    private static final String COLUMN_EMAIL_TYPE = "TEXT";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_MOBILE_NUMBER_TYPE = "TEXT";
    private static final String COLUMN_MOBILE_NUMBER = "mobile_number";
    private static final String COLUMN_IMGURL_TYPE = "TEXT";
    private static final String COLUMN_IMGURL = "imageUrl";
    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " " + COLUMN_ID_TYPE + COMMA_SEP +
                    COLUMN_NAME  + " " + COLUMN_NAME_TYPE + COMMA_SEP +
                    COLUMN_SURNAME + " " + COLUMN_SURNAME_TYPE + COMMA_SEP +
                    COLUMN_NICKNAME + " " + COLUMN_NICKNAME_TYPE + COMMA_SEP +
                    COLUMN_EMAIL + " " + COLUMN_EMAIL_TYPE + COMMA_SEP +
                    COLUMN_MOBILE_NUMBER + " " + COLUMN_MOBILE_NUMBER_TYPE + COMMA_SEP +
                    COLUMN_IMGURL + " " + COLUMN_IMGURL_TYPE +
                    ") WITHOUT ROWID";

    public static List<Contact> getAllContacts(Context context) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase db = databaseWrapper.getWritableDatabase();

        Cursor cur = db.rawQuery("SELECT * FROM " + ContactORM.TABLE_NAME, null);
        Log.i(TAG, "Loaded " + cur.getCount() + " Contacts");

        List<Contact> list = new ArrayList<>();

        if (cur.getCount() > 0) {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                Contact contact = cursorToContact(cur);
                list.add(contact);
                cur.moveToNext();
            }
            Log.i(TAG, "Contacts loaded successfully.");
        }

        db.close();
        return list;
    }

    public static void clearContacts(Context context) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase db = databaseWrapper.getWritableDatabase();

        /* clear `contacts` table */
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    public static void bulkInsertContacts(Context context, List<Contact> contacts) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase db = databaseWrapper.getWritableDatabase();
        db.beginTransaction();

        /*  For-each or "for enhanced" loop uses iterators in fancy way
         *  not showing them in code. This produces ConcurrentModificationException (logical)
         *  So use classic for loop for this stuff. Not a rocket science or optimization problem.
         *  A few milisec off is worth not locking variable in
         *  synchronized fashion and losing maybe even seconds in performance */
        for (int i = 0; i < contacts.size(); i++) {
            Contact contact = contacts.get(i);
            ContentValues values = ContactORM.contactToContentValues(contact);
            db.insertWithOnConflict(ContactORM.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    /* convert Contact to ContentValues */
    private static ContentValues contactToContentValues(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(ContactORM.COLUMN_EMAIL, contact.getEmail());
        values.put(ContactORM.COLUMN_ID, contact.getId());
        values.put(ContactORM.COLUMN_MOBILE_NUMBER, contact.getMobileNumber());
        values.put(ContactORM.COLUMN_NAME, contact.getName());
        values.put(ContactORM.COLUMN_SURNAME, contact.getSurname());
        values.put(ContactORM.COLUMN_NICKNAME, contact.getNickname());
        values.put(ContactORM.COLUMN_IMGURL, contact.getImgUri());
        return values;
    }

    @NonNull
    private static Contact cursorToContact(Cursor cur) {
        return new Contact(
                    cur.getLong(cur.getColumnIndex(ContactORM.COLUMN_ID)),
                    cur.getString(cur.getColumnIndex(ContactORM.COLUMN_NAME)),
                    cur.getString(cur.getColumnIndex(ContactORM.COLUMN_SURNAME)),
                    cur.getString(cur.getColumnIndex(ContactORM.COLUMN_NICKNAME)),
                    cur.getString(cur.getColumnIndex(ContactORM.COLUMN_EMAIL)),
                cur.getString(cur.getColumnIndex(ContactORM.COLUMN_MOBILE_NUMBER)),
                cur.getString(cur.getColumnIndex(ContactORM.COLUMN_IMGURL))
                );
    }
}
