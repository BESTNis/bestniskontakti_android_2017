package rs.bestnis.dusanknezevic.bestniskontakti.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import rs.bestnis.dusanknezevic.bestniskontakti.R;
import rs.bestnis.dusanknezevic.bestniskontakti.fragments.ContactsFragment;
import rs.bestnis.dusanknezevic.bestniskontakti.fragments.LoginFragment;
import rs.bestnis.dusanknezevic.bestniskontakti.fragments.SelectActionFragment;
import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;
import rs.bestnis.dusanknezevic.bestniskontakti.models.User;
import rs.bestnis.dusanknezevic.bestniskontakti.models.database.UserORM;
import rs.bestnis.dusanknezevic.bestniskontakti.services.DataService;
import rs.bestnis.dusanknezevic.bestniskontakti.services.networking.ConnectivityCheck;

public class MainActivity extends AppCompatActivity implements LoginFragment.LoginFragmentInteractionListener,
                    ContactsFragment.ContactsFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataService.getInstance().setApplicationContext(this.getApplicationContext()); //save main context. DataService will be saving it as App context

        FragmentManager fm = getSupportFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.container_main);

        if (currentFragment == null) {
            currentFragment = LoginFragment.newInstance();
            fm.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.fade_in, android.R.anim.fade_out)
                    .add(R.id.container_main, currentFragment).commit();
        }
    }

    @Override
    public void onLoginFragmentInteraction(boolean isLogged, User user) {
        if (isLogged) {
            FragmentTransaction fmt = getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.container_main, ContactsFragment.newInstance())
                    .addToBackStack(null);
            if (ConnectivityCheck.getConnectivityStatus(DataService.getInstance().getApplicationContext())) {
                UserORM.insertUser(DataService.getInstance().getApplicationContext(), user);
                fmt.commit();
            } else if (UserORM.userExists(getApplicationContext(), user.getUsername(), user.getPassword()))
                fmt.commit();
            else showToast(getResources().getString(R.string.no_user_local_db));
        } else showToast(getResources().getString(R.string.false_login_or_data));
    }

    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onContactsFragmentInteraction(Contact contact) {
        SelectActionFragment selectActionFragment = SelectActionFragment.newInstance(contact);
        selectActionFragment.show(getSupportFragmentManager(), "dialog");
    }
}
