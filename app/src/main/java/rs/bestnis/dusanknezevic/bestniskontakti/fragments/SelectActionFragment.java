package rs.bestnis.dusanknezevic.bestniskontakti.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import rs.bestnis.dusanknezevic.bestniskontakti.R;
import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;

public class SelectActionFragment extends DialogFragment implements View.OnClickListener {
    public static final String CONTACT_PARCELABLE_KEY = "contact";
    private static final short PERMISSION_WRITE_CONTACTS = 101;
    private static final short PERMISSION_CALL_PHONE = 102;

    public static SelectActionFragment newInstance(Contact contact) {
        SelectActionFragment frag = new SelectActionFragment();
        Bundle args = new Bundle();
        args.putParcelable(CONTACT_PARCELABLE_KEY, contact);
        frag.setArguments(args);
        return frag;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_contact_action, null);
        final ImageButton callBtn = v.findViewById(R.id.callContact);
        final ImageButton sendSmsBtn = v.findViewById(R.id.sendSmsContact);
        final ImageButton addContactBtn = v.findViewById(R.id.addToContacts);
        callBtn.setOnClickListener(this);
        sendSmsBtn.setOnClickListener(this);
        addContactBtn.setOnClickListener(this);

        builder.setView(v)
                .setTitle(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)).getName() + ' ' + ((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)).getSurname())
                .setNegativeButton("Nazad", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.callContact:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);
                } else {
                    SelectActionFragmentController.callPhone(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)));
                    dismiss();
                }
                break;
            case R.id.sendSmsContact:
                SelectActionFragmentController.sendSms(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)));
                break;
            case R.id.addToContacts:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS}, PERMISSION_WRITE_CONTACTS);
                } else {
                    SelectActionFragmentController.addToContacts(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)));
                    dismiss();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_WRITE_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    SelectActionFragmentController.addToContacts(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)));
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "Morate dati dozvolu za pristup kontaktima ako zelite dodati kontakt", Toast.LENGTH_LONG).show();
                }
                break;
            case PERMISSION_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SelectActionFragmentController.callPhone(((Contact) getArguments().getParcelable(CONTACT_PARCELABLE_KEY)));
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "Morate dati dozvolu za pozivanje kontakta ako zelite pozvati kontakt", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
