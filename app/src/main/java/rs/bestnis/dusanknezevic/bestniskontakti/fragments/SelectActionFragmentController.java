package rs.bestnis.dusanknezevic.bestniskontakti.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;
import rs.bestnis.dusanknezevic.bestniskontakti.services.DataService;


@SuppressWarnings({"MissingPermission"}) //they are handled within Fragment itself
public class SelectActionFragmentController {

    public static void callPhone(Contact contact) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getMobileNumber()));
        DataService.getInstance().getApplicationContext().startActivity(intent);
    }

    public static void addToContacts(Contact contact) {
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contact.getMobileNumber()));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = DataService.getInstance().getApplicationContext().getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);

        try {
            //if this is true, contact exists
            if (cur != null && cur.moveToFirst()) {
                long contactId = cur.getLong(cur.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                //lookup contact with given contactId from Contacts
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                DataService.getInstance().getApplicationContext().startActivity(intent);
                return;
            }
        } finally {
            if (cur != null)
                cur.close();
        }

        //if not found add to contacts
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, contact.getMobileNumber())
                .putExtra(ContactsContract.Intents.Insert.NAME, contact.getName() + " " + contact.getSurname())
                .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .putExtra(ContactsContract.Intents.Insert.EMAIL, contact.getEmail())
                .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .setType(ContactsContract.RawContacts.CONTENT_TYPE);
        DataService.getInstance().getApplicationContext().startActivity(intent);
    }

    public static void sendSms(Contact contact) {
        DataService.getInstance().getApplicationContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", contact.getMobileNumber(), null)));
    }
}
