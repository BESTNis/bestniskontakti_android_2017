package rs.bestnis.dusanknezevic.bestniskontakti.services.networking;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import rs.bestnis.dusanknezevic.bestniskontakti.services.DataService;

public class VolleySingleton {

    private static VolleySingleton mInstance = new VolleySingleton();
    private RequestQueue mRequestQueue;


    private VolleySingleton() {
        this.mRequestQueue = this.getRequestQueue();
    }

    public static VolleySingleton getInstance() {
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(DataService.getInstance().getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
