package rs.bestnis.dusanknezevic.bestniskontakti.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import rs.bestnis.dusanknezevic.bestniskontakti.R;
import rs.bestnis.dusanknezevic.bestniskontakti.adapters.ContactsAdapter;
import rs.bestnis.dusanknezevic.bestniskontakti.models.Contact;


public class ContactViewHolder extends RecyclerView.ViewHolder {
    public final ImageView mImageView;
    private TextView mContactName;
    private TextView mContactSurname;
    private TextView mContactNumber;

    public ContactViewHolder(View itemView) {
        super(itemView);

        this.mImageView = itemView.findViewById(R.id.contactImage);
        this.mContactName = itemView.findViewById(R.id.contactName);
        this.mContactSurname = itemView.findViewById(R.id.contactSurname);
        this.mContactNumber = itemView.findViewById(R.id.contactNumber);
    }

    public void updateUI(final Contact contact) {
        this.mContactName.setText(contact.getName());
        this.mContactSurname.setText(contact.getSurname());
        this.mContactNumber.setText(contact.getMobileNumber());
    }

    public void bind(final Contact contact, final ContactsAdapter.RecyclerViewOnItemClickListener listener) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(contact);
            }
        });
    }
}
