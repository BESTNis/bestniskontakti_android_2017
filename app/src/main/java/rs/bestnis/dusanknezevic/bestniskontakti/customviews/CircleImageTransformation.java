package rs.bestnis.dusanknezevic.bestniskontakti.customviews;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

/**
 * @link - https://androidmads.blogspot.rs/2017/06/circle-image-view-using-glide-and.html
 */

public class CircleImageTransformation implements Transformation {
    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;


        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        boolean sourceRecycled = false;
        if (squaredBitmap != source) {
            source.recycle();
            sourceRecycled = true;
        }

        /* Don't call getConfig() on recycled source. - Dusan K. <duki994@gmail.com>
           Based on alxscms@github comment - https://gist.github.com/julianshen/5829333 */
        Bitmap.Config config = sourceRecycled ? Bitmap.Config.ARGB_8888 : source.getConfig();
        Bitmap bitmap = Bitmap.createBitmap(size, size, config);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();
        return bitmap;
    }


    @Override
    public String key() {
        return "circle";
    }
}
