package rs.bestnis.dusanknezevic.bestniskontakti.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rs.bestnis.dusanknezevic.bestniskontakti.R;
import rs.bestnis.dusanknezevic.bestniskontakti.models.User;
import rs.bestnis.dusanknezevic.bestniskontakti.services.DataService;
import rs.bestnis.dusanknezevic.bestniskontakti.services.networking.ConnectivityCheck;
import rs.bestnis.dusanknezevic.bestniskontakti.services.networking.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.LoginFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, DataService.LoginHandleInterface {

    private LoginFragmentInteractionListener mListener;

    private EditText mEmail;
    private EditText mPassword;
    private Button mLoginBtn;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment LoginFragment.
     */
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        this.mEmail = v.findViewById(R.id.login_email);
        this.mPassword = v.findViewById(R.id.login_password);
        this.mLoginBtn = v.findViewById(R.id.login_button);
        this.mLoginBtn.setOnClickListener(this);
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        DataService.getInstance().unregisterListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        DataService.getInstance().registerListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginFragmentInteractionListener) {
            mListener = (LoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LoginFragmentInteractionListener");
        }
        DataService.getInstance().registerListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        DataService.getInstance().unregisterListener(this);
    }

    @Override
    public void onClick(View v) {
            if (v == this.mLoginBtn) {
                setupData();
            }
    }

    public void setupData() {
        String email = this.mEmail.getText().toString();
        String password = this.mPassword.getText().toString();
        if (ConnectivityCheck.getConnectivityStatus(this.getActivity().getApplicationContext())) {
            if (isValidEmail(email) && isValidPassword(password)) {
                Log.d("LoginFragment", "LoginButtonClicked");
                JSONObject loginObj = new JSONObject();
                try {
                    loginObj.put("username", this.mEmail.getText().toString());
                    loginObj.put("password", this.mPassword.getText().toString());
                    VolleySingleton.getInstance().addToRequestQueue(DataService.getInstance().returnRequest(loginObj));
                } catch (JSONException e) {
                    Log.d("LoginFragment", "JSONException loginObj: " + e.getMessage());
                }
            }
        } else {
            DataService.getInstance().handleData();
        }
    }

    /**
     *
     * @param email - email to validate
     * @return boolean - returns true if email is validated by regex email pattern
     */
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     *
     * @param pass - password string to validate
     * @return boolean - return if pass valid
     */
    private boolean isValidPassword(String pass) {
        return pass != null && pass.length() > 2;
    }

    @Override
    public void handleLogin(boolean isLogged) {
        User user = new User(this.mEmail.getText().toString(), this.mPassword.getText().toString());
        this.mListener.onLoginFragmentInteraction(isLogged, user);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface LoginFragmentInteractionListener {
        void onLoginFragmentInteraction(boolean isLogged, User user);
    }

}
