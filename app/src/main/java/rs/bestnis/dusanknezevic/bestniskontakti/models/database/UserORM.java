package rs.bestnis.dusanknezevic.bestniskontakti.models.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import rs.bestnis.dusanknezevic.bestniskontakti.models.User;

public final class UserORM {
    private static final String TABLE_NAME = "users";
    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String COMMA_SEP = ", ";
    private static final String COLUMN_EMAIL_TYPE = "TEXT";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD_TYPE = "TEXT";
    private static final String COLUMN_PASSWORD = "password";

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " ("
                    + COLUMN_EMAIL + " " + COLUMN_EMAIL_TYPE + COMMA_SEP
                    + COLUMN_PASSWORD + " " + COLUMN_PASSWORD_TYPE
                    + ")";

    public static boolean userExists(Context context, String email, String password) throws Resources.NotFoundException {
        DatabaseWrapper wrapper = new DatabaseWrapper(context);
        SQLiteDatabase db = wrapper.getWritableDatabase();

        Cursor cur = db.rawQuery("SELECT * FROM " + UserORM.TABLE_NAME + " WHERE " + COLUMN_EMAIL + "='" + email + "'" + " AND " + COLUMN_PASSWORD + "='" + password + "'", null);
        if (cur.getCount() == 1) {
            cur.moveToFirst();
            db.close();
            return true;
        } else {
            db.close();
            return false;
        }
    }

    public static void insertUser(Context context, User user) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase db = databaseWrapper.getWritableDatabase();
        ContentValues userCntVal = userToContentValues(user);
        db.beginTransaction();
        db.insertWithOnConflict(TABLE_NAME, null, userCntVal, SQLiteDatabase.CONFLICT_REPLACE);
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    private static ContentValues userToContentValues(User user) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, user.getUsername());
        values.put(COLUMN_PASSWORD, user.getPassword());
        return values;
    }
}
